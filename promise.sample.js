// https://youtu.be/ho5PnBOoacw?t=666

// raw code
async function processAllUsers () {
    const sql = 'SELECT id FROM users';;
    const users = await db.query(sql, []);
    for (const user of users) {
        await processUser(user.id);
    }
}

// refactoring code
function processAllUsers () {
    const sql = 'SELECT id FROM users';

    return db.query(sql, [])
        .then(users => 
            waitForEach(user => processUser(user.id), users) );
}

const waitForEach = (processFunc, [head, ...tail]) => 
    !head
        ? Promise.resolve()
        : processFunc(head).then(waitForEach(processFunc, tail))